<?php namespace PizzaShop\Pizza;

/**
 * A cheese and tomato pizza.
 */
class CheeseAndTomato
{

    /**
     * Time for pizza to cook
     * @var integer
     */
    public $time = 20;

    /**
     * Label of this pizza
     * @var string
     */
    public $label = 'Cheese and Tomato';

    /**
     * Make pizza.
     *
     * @return $this
     */
    public function make()
    {
        return $this
            ->rollOutDough()
            ->spreadTomatoSauce()
            ->addTomatoes()//<--- This will change
            ->sprinkleOnCheese();
    }


    public function rollOutDough()
    {
        sleep(2);
        print 'Dough rolled out.'.PHP_EOL;

        return $this;
    }

    public function spreadTomatoSauce()
    {
        sleep(2);
        print 'Tomato sauce spread.'.PHP_EOL;

        return $this;
    }

    public function addTomatoes()
    {
        sleep(2);
        print 'Tomatoes added.'.PHP_EOL;

        return $this;
    }

    public function sprinkleOnCheese()
    {
        sleep(2);
        print 'Cheese sprinkled on.'.PHP_EOL;

        return $this;
    }
}