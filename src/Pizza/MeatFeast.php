<?php namespace PizzaShop\Pizza;

/**
 * A meat feast pizza.
 */
class MeatFeast
{

    /**
     * Time for pizza to cook
     * @var integer
     */
    public $time = 25;


    /**
     * Label of this pizza
     * @var string
     */
    public $label = 'Meat Feast';

    /**
     * Make pizza.
     *
     * @return $this
     */
    public function make()
    {
        return $this
            ->rollOutDough()
            ->spreadTomatoSauce()
            ->addChickenBeef()//<--- This will change
            ->sprinkleOnCheese();
    }


    public function rollOutDough()
    {
        sleep(2);
        print 'Dough rolled out.'.PHP_EOL;

        return $this;
    }

    public function spreadTomatoSauce()
    {
        sleep(2);
        print 'Tomato sauce spread.'.PHP_EOL;

        return $this;
    }

    public function addChickenBeef()
    {
        sleep(2);
        print 'Chicken & Beef pieces added.'.PHP_EOL;

        return $this;
    }

    public function sprinkleOnCheese()
    {
        sleep(2);
        print 'Cheese sprinkled on.'.PHP_EOL;

        return $this;
    }
}