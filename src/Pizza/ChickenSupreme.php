<?php namespace PizzaShop\Pizza;

/**
 * A chicken supreme pizza.
 */
class ChickenSupreme
{

    /**
     * Time for pizza to cook
     * @var integer
     */
    public $time = 22;

    /**
     * Label of this pizza
     * @var string
     */
    public $label = 'Chicken Supreme';

    /**
     * Make pizza.
     *
     * @return $this
     */
    public function make()
    {
        return $this
            ->rollOutDough()
            ->spreadTomatoSauce()
            ->addChicken()//<--- This will change
            ->sprinkleOnCheese();
    }


    public function rollOutDough()
    {
        sleep(2);
        print 'Dough rolled out.'.PHP_EOL;

        return $this;
    }

    public function spreadTomatoSauce()
    {
        sleep(2);
        print 'Tomato sauce spread.'.PHP_EOL;

        return $this;
    }

    public function addChicken()
    {
        sleep(2);
        print 'Chicken pieces added.'.PHP_EOL;

        return $this;
    }

    public function sprinkleOnCheese()
    {
        sleep(2);
        print 'Cheese sprinkled on.'.PHP_EOL;

        return $this;
    }
}