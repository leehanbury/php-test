<?php


namespace PizzaShop\Oven;

class StoneOven
{

    public function bake($pizza)
    {
        $time = $pizza->time - 10;
        sleep(2);
        print "Baked your $pizza->label pizza in stone oven for $time minutes";
    }
}