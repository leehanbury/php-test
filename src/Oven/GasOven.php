<?php


namespace PizzaShop\Oven;

class GasOven
{

    public function bake($pizza)
    {
	    	$time = $pizza->time;
	    	sleep(2);
        print "Baked your $pizza->label pizza in gas oven for $time minutes";
    }
}