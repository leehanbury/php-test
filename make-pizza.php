<?php

require 'vendor/autoload.php';

// $split = "----------------------------------------\n";
$split = "*****************************************\n";
$no_pizza = 'This pizza is not in our menu, please choose one we have available.';
$no_oven = 'This oven is not one that we have, please choose one we have available.';
// var_dump($argc);
// var_dump($argv);

//Choose type of pizza at run time.
print "$split"."--- Please enter your pizza number: ---\n$split";
// $the_pizzas = glob('src/Pizza/*.php', GLOB_NOSORT);
// foreach ($the_pizzas as $pizza_class) {
// 	if (strpos(file_get_contents("./".$pizza_class), )) {
// 	}
// }
$menu = array(
	1 => array( 'label' => 'Cheese and Tomato', 'class' => '\PizzaShop\Pizza\CheeseAndTomato' ),
	2 => array( 'label' => 'Chicken Supreme', 'class' => '\PizzaShop\Pizza\ChickenSupreme' ),
	3 => array( 'label' => 'Meat Feast', 'class' => '\PizzaShop\Pizza\MeatFeast' )
);
foreach ($menu as $pizza => $data) {
	print "#$pizza. " . $data['label'] . "\n";
}
$pizza_input = fopen('php://stdin','r');
$pizza_choice = trim(fgets($pizza_input));
if (!array_key_exists($pizza_choice, $menu)) {
	die($no_pizza);
}
if (class_exists($menu[$pizza_choice]['class'])) {
	$pizza = new $menu[$pizza_choice]['class'];
} else {
	die($no_pizza);
}
print "$split"."------- " . $menu[$pizza_choice]['label'] . " pizza -------\n$split";

//Make the pizza.
$pizza->make();

//Choose type of oven at run time.
print "$split"."----- Enter your oven number: -----\n$split";
$ovens = array(
	1 => array( 'label' => 'Gas Oven', 'class' => '\PizzaShop\Oven\GasOven' ),
	2 => array( 'label' => 'Hair Dryer', 'class' => '\PizzaShop\Oven\HairDryer' ),
	3 => array( 'label' => 'Stone Oven', 'class' => '\PizzaShop\Oven\StoneOven' )
);
foreach ($ovens as $type => $data) {
	print "#$type. " . $data['label'] . "\n";
}
$oven_input = fopen('php://stdin','r');
$oven_choice = trim(fgets($oven_input));
if (!array_key_exists($oven_choice, $ovens)) {
	die($no_oven);
}
if (class_exists($ovens[$oven_choice]['class'])) {
	$oven = new $ovens[$oven_choice]['class'];
} else {
	die($no_oven);
}
print "$split"."-------- You chose a " . $ovens[$oven_choice]['label'] . " --------\n$split";

//Bake the pizza.
$oven->bake($pizza);
die("\nEnjoy your Pizza!");